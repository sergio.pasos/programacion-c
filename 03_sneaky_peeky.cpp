#include <stdio.h>
#include <stdlib.h>

int main() {

	/* DECLARACIÓN DE VARIABLES */

    	int op1, op2, result;
	
	system ("clear");
	system("toilet -fpagga --gay CALCULADORA");
	printf("\n\n");


	/* ENTRADA DE DATOS */

	printf("\t-SUMA DE NÚMEROS-\n");
		
	printf("\n Este es el primer número a sumar: "); scanf("%i", &op1);
	
	printf("\n Este es el segundo número a sumar: "); scanf("%i", &op2);

	/* CÁLCULOS */
	 
	result = op1 + op2;

	/* SALIDA DE DATOS */

	printf("\n El número es: %i\n", result );
	
	printf("\n Dirección de memoria del dato 1: %p\n", &op1);
	
	printf("\n Dirección de memoria del dato 2: %p\n", &op2);	
	

	return EXIT_SUCCESS; 

}	

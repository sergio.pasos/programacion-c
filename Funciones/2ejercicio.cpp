#include <stdio.h>
#include <stdlib.h>

  int suma (int op1, int op2){
  
    return op1 + op2; //Operacion de sumar.
	  
  }

  int main(){

    int op1;
    int op2;

      printf("Escoje el primer operando: "); //Preguntar primer operando.
      scanf("%i", &op1);
      printf("Escoje el segundo operando: "); // Preguntar segundo operando.
      scanf("%i", &op2);

      printf("La suma es: %i\n", suma(op1,op2)); //Devuelve la suma hecha en la funcion suma.

  return 0;
}

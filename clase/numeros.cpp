#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

int imprimir (int ini, int fin) {
        if ( ini < fin ){
        printf("  %d", ini);
        ini=ini+1;
        imprimir (ini,fin);
        }
}

int  main () {
	
 int inicio;
 int fin;

 printf("INICIO: ");
 scanf("%d", &inicio);

 __fpurge(stdin);

 printf("FINAL: ");
 scanf("%d", &fin);

 imprimir (inicio, fin);
 return 0;

}



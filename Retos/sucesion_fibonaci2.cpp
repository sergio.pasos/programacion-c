#include <stdio.h>
#include <stdlib.h>

#define MAX 0x10

  int main(){
  
  unsigned long int fibonacci[MAX];

  fibonacci[1] = fibonacci[0] = 1;

  for (int i=2; i<MAX; i++){
      fibonacci[i] = fibonacci[i-1] + fibonacci[i-2]; 
}

  
  for (int i=0; i<MAX; i++){
      printf("%lu \n", fibonacci[i]);

}
  return 0;
}

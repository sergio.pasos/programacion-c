#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main () {

	char letra;

	printf ("Escribe una letra: ");
	scanf("%c", &letra);

	printf("%c\n",  toupper(letra));

	return 0;
}

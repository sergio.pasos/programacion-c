#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define MAX 3
#define VACIO 0

int main (){

	unsigned tablero[MAX][MAX];

	for(int f=0; f<MAX; f++){
		for(int c=0; c<MAX; c++){
			tablero[f][c]=VACIO;	
		}
	}

	//HASTA AQUÍ GUARDO CEROS EN EL ARRAY, QUE SERÁN LAS POSICIONES DE LAS CASILLAS.

	for(int f=0; f<MAX; f++){
		for(int c=0; c<MAX; c++){	
			if(c==0){
				printf("|%i", tablero[f][c]);
			}
			if(c==1){
				printf("|%i|", tablero[f][c]);
			}
			if(c==2){
				printf("%i", tablero[f][c]);
				printf("|\n");
			}		
		} 
	}
	//HASTA AQUÍ IMPRIMO LAS CASILLAS DEL TABLERO

	return 0;
}

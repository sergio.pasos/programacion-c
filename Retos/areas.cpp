#include <stdio.h>
#include <stdlib.h>

#define PI 3.14159265358979323846


const char *frase [] = {
	NULL,
	"Triángulo",
	"Círculo",
	"Cuadrado",
	"Rectángulo",
	"Rombo",
	"Pentágono",
	NULL,
};

void menu (){

	system ("clear");
    	system ("toilet -Fborder -fpagga --gay 'CALCULADORA DE ÁREAS'");
  	printf ("\n");

	printf ("Por favor, ingrese una de las siguientes opciones, según el número.\n");

	for (int i=1; i<=6; i++){
		printf ("%i) Calcular el área del %s\n", i, frase [i]);
	}
}

int preguntar_opt (int *opt){

	printf ("Que opción escojes: ");
	scanf ("%i", opt);

	return *opt;
}

void triangulo () {

	float base;
	float altura;
	float operacion;

	printf ("Proporcionanos la base del triángulo: ");
	scanf ("%f", &base);
	printf ("Proporcionanos su altura del triángulo: ");
	scanf ("%f", &altura);

	operacion = base * altura / 2;

	printf ("El área del triángulo es: %.2f\n", operacion);

}

void circulo () {

	float radio;
	float operacion;

	printf ("Proporcionanos el radio del círculo: ");
	scanf ("%f", &radio);

	operacion = radio * radio * PI;

	printf ("El área del círculo es: %.2f\n", operacion);
}

void cuadrado () {
	
	float lado;
	float cuadrado;

	printf("Proporcionanos el lado de tu cuadrado: ");
	scanf("%f", &lado);

	cuadrado = lado * lado;

	printf("El area de tu cuadrado es: %f\n", cuadrado);
}

void rectangulo () {
	
	float base;
	float altura;
	float operacion;

	printf ("Proporcionanos la base del rectángulo: ");
	scanf ("%f", &base);
	printf ("Proporcionanos la altura del rectángulo: ");
	scanf ("%f", &altura);

	operacion = base * altura;

	printf ("El área del rectángulo es: %.2f\n", operacion);
}

void rombo () {
	
	float diagonalM;
	float diagonalm;
	float operacion;
	
        printf ("Proporcionanos la diagonal mayor del rombo: ");
        scanf ("%f", &diagonalM);
        printf ("Proporcionanos la diagonal menor del rombo: ");
        scanf ("%f", &diagonalm);

	operacion = diagonalM * diagonalm / 2;

	printf ("El área del rombo es: %.2f\n", operacion);
}

void pentagono () {

	float lado;
	float apotema;
	float operacion;
	float perimetro;

	printf ("Proporciona lo que mide un lado del pentágono: ");
	scanf ("%f", &lado);
	printf ("Proporciona lo que mide la apotema del pentágono: ");
	scanf ("%f", &apotema);

	perimetro = lado * 5;

	operacion = perimetro * apotema / 2;

	printf ("El área del pentágono es: %.2f\n", operacion);
}

int main () {

	int opt;

	menu ();

	do{
	preguntar_opt (&opt);
	}
	while (opt < 1 || opt > 6);

	switch (opt){

		case 1:
			triangulo ();	
	 break;

		case 2:
			circulo ();
	 break;
		case 3:
                        cuadrado ();
         break;

                case 4:
                        rectangulo ();
	break;
		case 5:
			rombo ();
	break;
		case 6:
			pentagono ();
	} 

return 0;

}	

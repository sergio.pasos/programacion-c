#include <stdlib.h>
#include <stdio.h>

  int main(){

  double precio_coste;
  double margen;

    printf("Introduzca el precio de coste: "); //Preguntar el precio de coste al usuario.
    scanf("%lf", &precio_coste); //Guardamos el dato que nos pasa el usuario.

    printf("Introduzca el margen en un porcentaje del 1-100: "); //Preguntar el margen al usuario.
    scanf("%lf", &margen); //Guardamos el dato que nos pasa el usuario.

  float precio_neto = precio_coste * ((100 + margen) / 100); // Operación para calcular el precio_neto.

    printf("El precio total es: %f\n", precio_neto); // Imprimimos el resultado que nos de la operacion del precio_neto.

  return 0;
}

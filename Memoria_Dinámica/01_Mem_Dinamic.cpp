#include <stdlib.h>
#include <stdio.h>

int main (int argc, char *argv[]) {

	int *p = NULL;

	p = (int *) malloc (4);

	*p = 7;

	printf("%p: %i\n", p, *p);

	free (p);

	return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "interfaz.h"
#include "algoritmia.h"
#include "general.h"

#define MAX 8




void posicionar_fichas (enum TPieza tablero_negras[MAX][MAX]){

	for(int f=0; f<MAX; f++){
		for(int c=0; c<MAX; c++){
			if (c % 2 == 0 && (f==0 || f==1 || f== 6 || f==7))
				tablero_negras[f][c] = peones;
			else if (c % 2 != 0 && (f==0 || f==1 || f== 6 || f==7))
				tablero_negras[f][c] = caballo;
		}
	}
}


void ver(enum TPieza tablero_negras[MAX][MAX]){
	for (int f = 0; f < MAX; ++f){
		for (int c = 0; c < MAX; ++c){
			printf("%i ", tablero_negras[f][c]);
		}
		printf("\n");
	}
}


int main(){

	unsigned fila = 0;
	unsigned col = 0;
	int comprobado;
	enum TPieza tablero_negras[MAX][MAX];
	
	//	Rellenamos el array con 0
	memset(tablero_negras, 0, sizeof(tablero_negras));

	//	Posicionamos inicialmente las fichas	
	posicionar_fichas(tablero_negras);
	ver(tablero_negras);


	preguntar_posicion(&fila, &col);
	comprobado = comprobar_caballo(&tablero_negras[fila][col], fila, col);
	printf("%i", comprobado);
}
#include<stdio.h>
#include<stdlib.h>

#define FIL 8
#define COL 8

enum TPieza
{
	peones = 1,
	torres,
	caballos,
	alfiles,
	reinas,
	reyes
};

int main (){

	int tablero_negras[FIL][COL];

	for(int f=0; f<FIL; f++){
		for(int c=0; c<COL; c++){
			tablero_negras[f][c] = 0;		
		}}

	for(int f=0; f<FIL; f++){
		for(int c=0; c<COL; c++){
			printf("%i", tablero_negras[f][c]);
		}
		printf("\n");
	}

	return 0;
}

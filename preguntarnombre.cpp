#include <stdio.h> /* Declaración librerías*/
#include <stdlib.h>
#include <string.h>

int main (){

	char nombre[20];

	printf("Dime cuál es tu nombre: ");
	scanf("%s", nombre);

	printf("Hola %s, que tal estas?\n", nombre);

	return EXIT_SUCCESS;
}

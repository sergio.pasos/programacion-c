#include <stdio.h>
#include <stdlib.h>

int main() {

	/* DECLARACIÓN DE VARIABLES */

    	int op1, op2, result;
	
	/* ENTRADA DE DATOS */

	printf("\t-SUMA DE NÚMEROS-\n");
		
	printf("\n Este es el primer número a sumar: "); scanf("%i", &op1);
	
	printf("\n Este es el segundo número a sumar: "); scanf("%i", &op2);

	/* CÁLCULOS */
	 
	result = op1 + op2;

	/* SALIDA DE DATOS */

	printf("\n El número es: %i\n", result );


		
	return EXIT_SUCCESS; 

}	

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

    int main() {

    char respuesta;

      printf("¿Quieres instalar algo? Responde con 's(si)/n(no)': ");
      scanf("%c", &respuesta);

    if(tolower(respuesta) == 's'){ //Con el tolower, convertimos de mayúsculas a minúsculas antes de comprobar la condición.
      printf("Pues lo instalo\n");
    }
    else{
     if(tolower(respuesta) == 'n'){
      printf("Pues no lo instalo\n");
    }}
    
    return 0;
}
	

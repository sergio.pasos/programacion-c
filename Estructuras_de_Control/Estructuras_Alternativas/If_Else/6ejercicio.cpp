#include <stdio.h>
#include <stdlib.h>

#define E 0.001 //Aquí definimos que E siempre vale 0.001.

   int main(){

   float real; //Variable tipo real.

   printf("Dinos un número real por favor ");
   scanf("%f", &real);  
   
   if(real >=3-E && real <=3+E){ //Aquí especificamos el entorno.
	   printf("Se encuentra dentro del entorno\n");
   }
      else{
   	   printf("Esta fuera del entorno\n");
   }
   return 0;
}

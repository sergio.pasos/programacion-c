#include <stdio.h>
#include <stdlib.h>
#include <string.h>

  int main(){

  char frase[40];
  int longfrase; //Longitud de la frase.

    printf ("Escribe una frase: ");
    fgets (frase, 40, stdin);

    longfrase = strlen (frase); //Mide la cantidad de caracteres y lo mete en frase.

  while (longfrase != '\0' - 1) //Mientras longitud frase sea distinto al caracter de final de frase.
    {
      printf ("%c", frase[longfrase]);
      longfrase--;
    }

    printf ("\n");

  return 0;
}

#include <stdio.h>
#include <stdlib.h>

   int main(){

   char letra;
   int n;

        printf("Caracter a imprimir: "); // Pregunta el caracter a imprimir.
        scanf("%c", &letra);

        printf("Veces a repetir: "); // Pregunta las veces que se va a repetir.
        scanf("%i", &n);

   for(int i=0; i<n; i++) // Variable local i, el bucle termina cuando i valga menos que n y si el bucle se repite, incrementamos 1 en i. 

	printf("%c ", letra); // Imprime el caracter elegido.
        printf("\n");

  return 0;
}

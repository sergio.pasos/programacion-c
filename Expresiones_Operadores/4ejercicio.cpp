#include <stdio.h>
#include <stdlib.h>

   int main(){

	int a = 0; // Esto es una variable de tipo entero que le das el valor de 0.
	char b = 6; // Esto es una variable de tipo carácter que le das el valor de 6.

	while (b > 0) { // While se va a repetir en caso de que b sea mayor que 0.
   	 b <<= 1; // Si b es mayor que 0, desplaza b un bit hacia la izquierda.
	 b |= (a = a++ % 2) ; // En esta línea a siempre vale 0.

	return 0;
}
}


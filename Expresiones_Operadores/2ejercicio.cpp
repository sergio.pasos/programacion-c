#include <stdio.h>
#include <stdlib.h>

int main() {
   
    int numero[10];
    
    printf("División entre tamaño en bytes y lo que ocupa cada elemento es: %li\n", sizeof(numero)/sizeof(numero[0])); //Aquí dividimos el tamaño del array entre el tamaño de un elemento
    
    return 0;
}

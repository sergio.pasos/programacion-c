#include <stdio.h>
#include <stdlib.h>

int main() {

char operador;
int op1, op2;

	printf("Ingresa la operación a realizar(+, -, *, /)  ");
	scanf("%c", &operador);

	printf("Ingrese el Primer Operando   ");
        scanf("%i", &op1);

	printf("Ingresa el Segundo Operando   ");
        scanf("%i", &op2);


	switch (operador) {

case '+': 
	printf("%i + %i = %i\n", op1, op2, op1 + op2);
	break;

case '-':
        printf("%i - %i = %i\n", op1, op2, op1 - op2);   
        break;

case '*':
        printf("%i * %i = %i\n", op1, op2, op1 * op2);
        break;

case '/':
        printf("%i / %i = %i\n", op1, op2, op1 / op2);   
        break;

	default:
	printf("el operador no es correcto");

	}

}	
